//
//  ArticleDetailsViewController.swift
//  Shrzen
//
//  Created by Omar Adel on 11/19/19.
//  Copyright © 2019 z510. All rights reserved.
//

import UIKit

class ArticleDetailsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func CommentsPressed(_ sender: Any) {
        guard let popupNavController = storyboard?.instantiateViewController(withIdentifier: "CommentsViewController") as? CommentsViewController else { return }
        present(popupNavController, animated: true, completion: nil)

    }
    
}
