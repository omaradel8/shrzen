//
//  RecoveryPasswordViewController.swift
//  Shrzen
//
//  Created by Omar Adel on 11/21/19.
//  Copyright © 2019 z510. All rights reserved.
//

import UIKit

class RecoveryPasswordViewController: UIViewController {
    @IBOutlet weak var emailTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let imageView = UIImageView();
        let image = UIImage(named: "mail.png");
        imageView.image = image;
        emailTextField.leftView = imageView;
        emailTextField.leftViewMode = UITextField.ViewMode.always
        emailTextField.leftViewMode = .always
        
    }
    
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destVC = storyboard.instantiateViewController(withIdentifier: "EnterCodeVC") as! RecoveryPasswordEnterCodeViewController
        destVC.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(destVC, animated: true)
    }
    

}
