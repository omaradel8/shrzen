//
//  EditProfileViewController.swift
//  Shrzen
//
//  Created by Mohamed on 11/21/19.
//  Copyright © 2019 z510. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {
    let countryPicker = UIPickerView()
    let cityPicker = UIPickerView()
    
    let countries = ["German", "England", "France"]
    let cities = ["London", "Paris"]

    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        countryPicker.delegate = self
        countryPicker.dataSource = self
        cityPicker.delegate = self
        cityPicker.dataSource = self
        createCountriesPicker()
        createCityPicker()
        showspecialtiesPicker()
        doneButtonForCitiesPicker()
                
    }
    


    
    func createCountriesPicker(){
        countryTextField.inputView = countryPicker
    }
    
    func createCityPicker(){
        cityTextField.inputView = cityPicker
    }

    
    
     func showspecialtiesPicker(){
           let toolbar = UIToolbar();
           toolbar.sizeToFit()
           let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: ""), style: .plain, target: self, action: #selector(donePicker));
           toolbar.setItems([doneButton], animated: false)
           toolbar.tintColor = #colorLiteral(red: 0.1357244253, green: 0.1270827949, blue: 0.1881380081, alpha: 1)
           countryTextField.inputAccessoryView = toolbar
       }
    
    @objc func donePicker(){
        
        if countryTextField.text == "" {
            countryTextField.text = countries[0]
        }
        countryTextField.endEditing(true)
    }
    
     func doneButtonForCitiesPicker(){
           let toolbar = UIToolbar();
           toolbar.sizeToFit()
           let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: ""), style: .plain, target: self, action: #selector(doneCityPicker));
           toolbar.setItems([doneButton], animated: false)
           toolbar.tintColor = #colorLiteral(red: 0.1357244253, green: 0.1270827949, blue: 0.1881380081, alpha: 1)
           cityTextField.inputAccessoryView = toolbar
       }
    
    @objc func doneCityPicker(){
        
        if cityTextField.text == "" {
            cityTextField.text = cities[0]
        }
        cityTextField.endEditing(true)
    }

}


extension EditProfileViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == countryPicker{
            return countries.count
        }else if pickerView == cityPicker{
            return cities.count
        }else{
            return 0
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == countryPicker{
            return countries[row]
        }else{
            return cities[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == countryPicker{
            countryTextField.text = countries[row]
        }else{
            cityTextField.text = cities[row]
        }
    }
    
    
    
}
