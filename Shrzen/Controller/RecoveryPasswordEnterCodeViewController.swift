//
//  RecoveryPasswordEnterCodeViewController.swift
//  Shrzen
//
//  Created by Mohamed on 11/21/19.
//  Copyright © 2019 z510. All rights reserved.
//

import UIKit
import CBPinEntryView

class RecoveryPasswordEnterCodeViewController: UIViewController {

    @IBOutlet weak var pinEntryView: CBPinEntryView! {
        didSet{
            pinEntryView.delegate = self as CBPinEntryViewDelegate
            pinEntryView.keyboardType = UIKeyboardType.numberPad.rawValue
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 12, *){
            pinEntryView.textContentType = .oneTimeCode
        }
    }

    @IBAction func didntReciveCodePressed(_ sender: Any) {
    }
    @IBAction func nextPresed(_ sender: Any) {
    }
    
}


extension RecoveryPasswordEnterCodeViewController: CBPinEntryViewDelegate{
    func entryChanged(_ completed: Bool) {
        print(pinEntryView.getPinAsString())
    }
    func entryCompleted(with entry: String?) {
        print(entry!)
    }
}
