//
//  SendMessageViewController.swift
//  Shrzen
//
//  Created by Omar Adel on 11/24/19.
//  Copyright © 2019 z510. All rights reserved.
//

import UIKit

class SendMessageViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var messageBodyTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        messageBodyTextView.delegate = self
        
    }
    
    
    @IBAction func sendButtonPressed(_ sender: Any) {
    }
    


}


//UItextView Delegate for PlaceHolder
extension SendMessageViewController: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
        textView.textColor = UIColor(hexString: "#474747")
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Message"
            textView.textColor = UIColor(hexString: "#ABABAB" )
        }
    }
}
