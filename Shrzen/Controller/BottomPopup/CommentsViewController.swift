//
//  CommentsViewController.swift
//  Shrzen
//
//  Created by Omar Adel on 11/19/19.
//  Copyright © 2019 z510. All rights reserved.
//

import UIKit
import BottomPopup

class CommentsViewController: BottomPopupViewController {
    @IBOutlet weak var commentArea: UITextView!
    
    @IBOutlet weak var commentsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commentsTableView.delegate = self
        commentsTableView.dataSource = self
        commentArea.delegate = self

        // Do any additional setup after loading the view.
    }
    
    
    override func getPopupHeight() -> CGFloat {
        return CGFloat(400)
    }
    
    override func getPopupTopCornerRadius() -> CGFloat {
        return CGFloat(10)
    }
    
    override func getPopupPresentDuration() -> Double {
        return 0.5
    }
    
    override func getPopupDismissDuration() -> Double {
        return 0.5
    }
    
    override func shouldPopupDismissInteractivelty() -> Bool {
        return true
    }
    
    @IBAction func dismisPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    

}

// MARK: - Bottompopup
extension CommentsViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath)
        
        
        return cell
    }
}


// MARK: - Text View Delegate
extension CommentsViewController: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
        textView.textColor = UIColor.black
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Write a comment .."
            textView.textColor = UIColor.lightGray
        }
    }
    
}
