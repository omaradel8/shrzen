//
//  AdSectionsViewController.swift
//  Shrzen
//
//  Created by Omar Adel on 11/19/19.
//  Copyright © 2019 z510. All rights reserved.
//

import UIKit

protocol getSectionsDelegate {
    func getSelectedSection(mainCategory : String,sections: [String])
}

class AdSectionsViewController: UIViewController {
    @IBOutlet weak var mainCategoryCollectionView: UICollectionView!
    
    var delegate: getSectionsDelegate?
    
    var MainCategoryIsSelected: Bool = false
    var selectedCategory = IndexPath()
    var selectedSub = [IndexPath]()
    var section = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainCategoryCollectionView.dataSource = self
        mainCategoryCollectionView.delegate = self
        
        mainCategoryCollectionView.allowsMultipleSelection = true

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        if let cell1 = mainCategoryCollectionView.cellForItem(at: selectedCategory) as? MainCategoryCell{
            for e in selectedSub{
                let cell = mainCategoryCollectionView.cellForItem(at: e) as! MainCategoryCell
                section.append(cell.categoryNameLabel.text!)
            }
            delegate?.getSelectedSection(mainCategory: cell1.categoryNameLabel.text!, sections: section)
        }
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func cancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}




// MARK: - Collection View Delegate and DataSource
extension AdSectionsViewController: UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 4
        }else {
            return 5
        }
    }
    

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if MainCategoryIsSelected{
                collectionView.deselectItem(at: selectedCategory, animated: true)
                let cell = collectionView.cellForItem(at: selectedCategory) as! MainCategoryCell
                cellDeslected(cell: cell)
            }
            let cell = collectionView.cellForItem(at: indexPath) as! MainCategoryCell
            cellSelected(cell: cell)
            MainCategoryIsSelected = true
            selectedCategory = indexPath
            
        }else if indexPath.section == 1{
            selectedSub.append(indexPath)
            let cell = collectionView.cellForItem(at: indexPath) as! MainCategoryCell
            cellSelected(cell: cell)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if indexPath.section == 0{
            let cell = collectionView.cellForItem(at: indexPath) as! MainCategoryCell
            cellDeslected(cell: cell)
        }else if indexPath.section == 1{
            if selectedSub.contains(indexPath) {
                if let num = selectedSub.firstIndex(of: indexPath){
                selectedSub.remove(at: num)
                }
            }
            let cell = collectionView.cellForItem(at: indexPath) as! MainCategoryCell
            cellDeslected(cell: cell)
        }
        collectionView.deselectItem(at: indexPath, animated: true)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainCategoryCell", for: indexPath) as! MainCategoryCell
            cell.categoryNameLabel.text = "Main"
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainCategoryCell", for: indexPath) as! MainCategoryCell
            cell.categoryNameLabel.text = "Sub"
            return cell
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionHeaderCollection", for: indexPath) as! SectionHeaderCollection
        if indexPath.section == 0 {
            sectionHeader.sectionHeaderlabel.text = "first"
        }else if indexPath.section == 1{
            sectionHeader.sectionHeaderlabel.text = "second"
        }
        
        return sectionHeader
    }
    
    func cellSelected(cell : UICollectionViewCell){
        let cell = cell as! MainCategoryCell
        cell.categoryNameLabel.backgroundColor = UIColor(hexString: "#C3812B")
        cell.categoryNameLabel.textColor = UIColor.white
    }
    
    func cellDeslected(cell : UICollectionViewCell){
        let cell = cell as! MainCategoryCell
        cell.categoryNameLabel.backgroundColor = UIColor(hexString: "#EAEAEB")
        cell.categoryNameLabel.textColor = UIColor(hexString: "#474747")
    }
    
    
    
}


extension AdSectionsViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 3 , left: 0, bottom: 3, right: 0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    


}
