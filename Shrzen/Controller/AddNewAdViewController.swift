//
//  AddNewAdViewController.swift
//  Shrzen
//
//  Created by Omar Adel on 11/19/19.
//  Copyright © 2019 z510. All rights reserved.
//

import UIKit

class AddNewAdViewController: UIViewController, getSectionsDelegate {

    
    @IBOutlet weak var ChoseImageView: UIImageView!
    
    
    @IBOutlet weak var adNameTextField: UITextField!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var sectionTextField: UITextField!
    @IBOutlet weak var choseSectionTextField: UITextField!
    @IBOutlet weak var priceLable: UILabel!
    @IBOutlet weak var contactNumberTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var addImageButton: UIButton!
    @IBOutlet weak var addViewForImage: UIView!
    @IBOutlet weak var deleteChosenImageButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sectionTextField.isEnabled = false
        deleteChosenImageButton.isEnabled = false
        deleteChosenImageButton.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func AddImageButtonPressed(_ sender: Any) {
        let image = UIImagePickerController()
        image.delegate = self
        
        image.sourceType = UIImagePickerController.SourceType.photoLibrary
        image.allowsEditing = false
        
        self.present(image,animated: true)
        
    }
    
    
    @IBAction func deleteImagePressed(_ sender: Any) {
        ChoseImageView.image = nil
        addImageButton.isEnabled = true
        addViewForImage.isHidden = false
        deleteChosenImageButton.isEnabled = false
        deleteChosenImageButton.isHidden = true
    }
    
    
    
    
    //MARK: - get the sections
    func getSelectedSection(mainCategory: String, sections: [String]) {
        choseSectionTextField.text = "\(mainCategory) ("
        for e in sections{
            choseSectionTextField.text?.append(contentsOf: "\(e) ")
        }
        choseSectionTextField.text?.append(contentsOf: ")")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SelectSectionSegue"{
            let destinationVC = segue.destination as! AdSectionsViewController
            destinationVC.delegate = self
        }
    }
    

}

extension AddNewAdViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            ChoseImageView.image = image
            addViewForImage.isHidden = true
            addImageButton.isEnabled = false
            deleteChosenImageButton.isEnabled = true
            deleteChosenImageButton.isHidden = false
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    


    
}


