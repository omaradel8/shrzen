//
//  MainCategoryCell.swift
//  Shrzen
//
//  Created by Omar Adel on 11/20/19.
//  Copyright © 2019 z510. All rights reserved.
//

import UIKit

class MainCategoryCell: UICollectionViewCell {
    @IBOutlet weak var categoryNameLabel: UILabel!
    
    override func awakeFromNib() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
    }
    
}
